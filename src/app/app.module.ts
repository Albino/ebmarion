import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { ServicesComponent } from './services/services.component';
import { ContactComponent } from './contact/contact.component';
import { AppRoutingModule } from './app-routing.module';
import { Ng2MapModule } from 'ng2-map';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ServicesComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    Ng2MapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyA5qY8ycWWLHXcV4HTjkN0J8Sy7dlx67Tg' +
      '&libraries=visualization,places,drawing',})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
