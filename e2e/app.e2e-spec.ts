import { NgMarionPage } from './app.po';

describe('ng-marion App', () => {
  let page: NgMarionPage;

  beforeEach(() => {
    page = new NgMarionPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
